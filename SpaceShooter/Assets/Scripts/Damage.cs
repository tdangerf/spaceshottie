﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour
{
    public float lifetime = 1.0f;
    public int damage = 5;
    void Start()
    {
    }
    void Update()
    {
    }
    void Awake()
    {
        Destroy(gameObject, lifetime);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            other.gameObject.GetComponent<Health>().CurrentHealth -= damage;
            DestroyObject(other.gameObject);
            Destroy(gameObject);
        }
    }
}